import React, {useState} from 'react';
import styles from './style.module.scss'
import { toast } from 'react-toastify';
import {useDispatch, useSelector} from "react-redux";
import {addTodo} from "../../redux/slices/todoSlice";


const AddTodoForm = () => {
    const [value, setValue] = useState('')
    const dispatch = useDispatch()
    const todos = useSelector(state => state.todos)

    const submitHandler = (e) => {
        e.preventDefault()
        if(todos.map(item => item.title).indexOf( value) != -1){
            toast.error('Такая задача уже существует')
        }else if(!value){
            toast.error('Значение не может быть пустым')
        }
        else{
            dispatch(addTodo({title: value, completed: false}))
            toast.success('Успешно добавлено')
            setValue('')
        }
    }

    return (
            <form onSubmit={e => submitHandler(e)} className={styles.form}>
                <input
                    type="text"
                    placeholder='Добавить задачу'
                    value={value}
                    onChange={e => setValue(e.target.value)}
                />
                <button type='submit'>Добавить</button>
            </form>
    );
};

export default AddTodoForm;