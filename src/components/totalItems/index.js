import React from 'react';
import styles from './style.module.scss'
import {useSelector} from "react-redux";

const TotalItems = () => {
    const total = useSelector(state => state.todos)
    return (
        <div>
         <span>Количество задач: {total.length}</span>
        </div>
    );
};

export default TotalItems;