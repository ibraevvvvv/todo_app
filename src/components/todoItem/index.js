import React, {useEffect, useState} from 'react';
import {toast} from "react-toastify";
import styles from './style.module.scss'
import {useDispatch} from "react-redux";
import {deleteTodo, toggleComplete, updateTodo} from "../../redux/slices/todoSlice";

const TodoItem = ({id, title, completed,created_at,updated_at = null}) => {
    const dispatch = useDispatch()
    const [state, setState] = useState(false)
    const [newTitle, setNewTitle] = useState('')

    useEffect(() => {
        setNewTitle(title)
    },[])

    const toggleItemHandler = () => {
        dispatch(toggleComplete({id: id, completed: !completed}))
    }

    const deleteItemHanlder = (e) => {
        e.preventDefault()
        toast.success('Успешно удалено')
        dispatch(deleteTodo({id: id}))
    }

    const editItemHandler = (e) => {
        e.preventDefault()
        dispatch(updateTodo({id: id, title: newTitle}))
        setState(false)
        toast.success('Успешно изменено')
    }

    const cancelBtn = (e) => {
        e.preventDefault()
        setState(!state)
        setNewTitle(title)
    }

    return (
        <li className={styles.todo_item} style={completed ? {'background': 'lightGreen'}: null}>
            {
                !state
                    ?
                    <div className={styles.todo_item_inner}>
                        <div>
                            <span>
                               <input
                                   type="checkbox"
                                   checked={completed}
                                   onChange={() => toggleItemHandler()}
                                   className={styles.input_checkbox}
                               />
                                {title}
                            </span>
                        </div>
                        <div>
                            <p>Добавлено в {created_at}</p>
                            {updated_at && <p>Изменено в {updated_at}</p>}
                        </div>
                        <div>
                            <button
                                onClick={() => setState(true)}
                                className={styles.editBtn}
                            >Изменить</button>
                        </div>

                        <button
                            onClick={e => deleteItemHanlder(e)}
                            className={styles.deleteBtn}
                        >Удалить</button>
                    </div>
                    :
                    <div  className={styles.todo_item_inner}>
                        <input
                            type="text"
                            className={styles.input_text}
                            value={newTitle}
                            onChange={e => setNewTitle(e.target.value)}
                        />
                        <button
                            className={styles.editBtn}
                            onClick={(e) => editItemHandler(e)}
                        >Изменить</button>
                        <button
                            className={styles.cancelBtn}
                            onClick={(e) => cancelBtn(e)}
                        >Отмена</button>
                    </div>

            }
        </li>
    );
};

export default TodoItem;