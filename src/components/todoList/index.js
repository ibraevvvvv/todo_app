import React from 'react';
import styles from './style.module.scss'
import TodoItem from "../todoItem";
import {useSelector} from "react-redux";

const TodoList = () => {
    const todos = useSelector(state => state.todos)
    return (
        <ul className={styles.todo_item}>
            {
                todos.map(item => (
                    <TodoItem key={item.id} {...item}/>
                ))
            }
        </ul>
    );
};

export default TodoList;