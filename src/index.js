import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './styles/global.module.scss'
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import store from './redux/store';
import {Provider} from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore } from 'redux-persist';

let persistor = persistStore(store);

ReactDOM.render(
  <React.StrictMode>
      <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
              <App />
          </PersistGate>
      </Provider>
      <ToastContainer
          position="top-right"
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
      />
  </React.StrictMode>,
  document.getElementById('root')
);

