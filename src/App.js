import AddTodoForm from "./components/addTodoForm";
import TodoList from "./components/todoList";
import TotalItems from "./components/totalItems";


function App() {
  return (
    <div className='container'>
        <div className='content'>
            <div>
                <h1>Мои Задачи</h1>
                <AddTodoForm/>
                <TodoList/>
                <TotalItems/>
            </div>
        </div>
    </div>
  );
}

export default App;
