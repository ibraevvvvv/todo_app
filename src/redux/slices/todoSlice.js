import {createSlice} from "@reduxjs/toolkit";

const todoSlice = createSlice({
    name: 'todos',
    initialState: [],
    reducers:{
        addTodo: (state, {payload}) => {
            const newTodo = {
                id: Date.now(),
                title: payload.title,
                completed: false,
                created_at: new Date().toLocaleTimeString(),
            };
            state.push(newTodo)
        },
        toggleComplete: (state, {payload}) => {
            const index = state.findIndex((todo) => todo.id === payload.id)
            state[index].completed = payload.completed
        },
        deleteTodo: (state, {payload}) => {
            return state.filter((todo) => todo.id !== payload.id)
        },
        updateTodo: (state, {payload}) => {
            const index = state.findIndex((todo) => todo.id === payload.id)
                state[index].title = payload.title;
                state[index].updated_at = new Date().toLocaleTimeString();
        }
    }
})

export const {
    addTodo,
    toggleComplete,
    deleteTodo,
    updateTodo,
} = todoSlice.actions;

export default todoSlice.reducer;